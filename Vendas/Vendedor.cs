﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class Vendedor
    {
        public int Id { set; get; }
        public string Nome { set; get; }
        public string Cpf { set; get; }
        public int Taxa { set; get; }
    }
}
