﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class Venda
    {
        public int Id { set; get; }
        public Cliente cliente { set; get; }
        public Vendedor Vendedor { set; get; }
        public DateTime DataVenda { set; get; }
        public List<ItemVenda> Itens { set; get; }        
    }
}
