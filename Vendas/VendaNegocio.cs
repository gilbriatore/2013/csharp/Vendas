﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class VendaNegocio
    {
        public static float CalcularTotal(List<ItemVenda> Itens)
        {
            float total = 0;
            foreach (ItemVenda x in Itens)
            {
                total += ItemVendaNegocio.CalcularSubTotal(x);
            }
            return total;
        }
    }
}
