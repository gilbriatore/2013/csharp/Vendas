﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class VendedorCollection
    {
        private static List<Vendedor> vendedores = new List<Vendedor>();

        public static void Insert(Vendedor vendedor)
        {
            vendedor.Id = vendedores.Count() + 1;
            vendedores.Add(vendedor);
        }

        public static Vendedor Search(Vendedor vendedor)
        {
            foreach (Vendedor x in vendedores)
            {
                if (x.Cpf.Equals(vendedor.Cpf))
                {
                    return x;
                }
            }
            return null;
        }
    }
}
