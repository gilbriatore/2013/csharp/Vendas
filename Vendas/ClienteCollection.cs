﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vendas
{
    class ClienteCollection
    {
        private static List<Cliente> clientes = new List<Cliente>();

        public static void Insert(Cliente cliente)
        {
            cliente.Id = clientes.Count() + 1;
            clientes.Add(cliente);
        }

        public static Cliente Search(Cliente cliente)
        {
            foreach (Cliente x in clientes)
            {
                if (x.Cpf.Equals(cliente.Cpf))
                {
                    return x;
                }
            }
            return null;
        }
    }
}
